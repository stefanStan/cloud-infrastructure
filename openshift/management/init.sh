#!/usr/bin/env bash

minishift profile set dev

minishift config set memory 40960MB
minishift config set cpus 8
minishift config set disk-size 1024G
minishift config set iso-url centos
minishift config set server-loglevel 6
minishift config set vm-driver virtualbox
minishift config set username admin
minishift config set password admin